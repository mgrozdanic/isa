package com.example.demo.dto;

import java.util.List;

public class FlightRealisationResultDto {
    private String from;
    private String fromCode;
    private String to;
    private String toCode;
    private TimeDto departureTime;
    private DateDto departureDate;
    private TimeDto arrivalTime;
    private DateDto arrivalDate;
    private String type;//one way/round
    private String classT;//econ business...
    private String company;
    private double price;
    private FlightDto flight;
    private List<Integer> unavailable;
    private int rows;
    private int columns;

    public FlightRealisationResultDto() {
    }

    public FlightRealisationResultDto(String from, String to, TimeDto departureTime, DateDto departureDate,
                                      TimeDto arrivalTime, DateDto arrivalDate, String type, String classT,
                                      String company, double price, FlightDto flight, List<Integer> unavailable,
                                      int rows, int columns, String fromCode, String toCode) {
        this.from = from;
        this.to = to;
        this.departureTime = departureTime;
        this.departureDate = departureDate;
        this.arrivalTime = arrivalTime;
        this.arrivalDate = arrivalDate;
        this.type = type;
        this.classT = classT;
        this.company = company;
        this.price = price;
        this.flight = flight;
        this.unavailable = unavailable;
        this.rows = rows;
        this.columns = columns;
        this.fromCode = fromCode;
        this.toCode = toCode;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public TimeDto getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(TimeDto departureTime) {
        this.departureTime = departureTime;
    }

    public DateDto getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(DateDto departureDate) {
        this.departureDate = departureDate;
    }

    public TimeDto getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(TimeDto arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public DateDto getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(DateDto arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClassT() {
        return classT;
    }

    public void setClassT(String classT) {
        this.classT = classT;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public FlightDto getFlight() {
        return flight;
    }

    public void setFlight(FlightDto flight) {
        this.flight = flight;
    }

    public List<Integer> getUnavailable() {
        return unavailable;
    }

    public void setUnavailable(List<Integer> unavailable) {
        this.unavailable = unavailable;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public String getFromCode() {
        return fromCode;
    }

    public void setFromCode(String fromCode) {
        this.fromCode = fromCode;
    }

    public String getToCode() {
        return toCode;
    }

    public void setToCode(String toCode) {
        this.toCode = toCode;
    }
}
