package com.example.demo.dto;

public class FlightRealisationDto {
    private FlightDto selectedFlight;
    private AircraftDto selectedPlane;
    private DateDto departureDate;
    private TimeDto departureTime;
    private DateDto arrivalDate;
    private TimeDto arrivalTime;
    private double firstClass;//
    private double business;
    private double economy;
    private String company;

    public FlightRealisationDto() {
    }

    public FlightRealisationDto(FlightDto selectedFlight, AircraftDto selectedPlane, DateDto departureDate,
                                TimeDto departureTime, DateDto arrivalDate, TimeDto arrivalTime, double firstClass,
                                double business, double economy, String company) {
        this.selectedFlight = selectedFlight;
        this.selectedPlane = selectedPlane;
        this.departureDate = departureDate;
        this.departureTime = departureTime;
        this.arrivalDate = arrivalDate;
        this.arrivalTime = arrivalTime;
        this.firstClass = firstClass;
        this.business = business;
        this.economy = economy;
        this.company = company;
    }

    public FlightDto getSelectedFlight() {
        return selectedFlight;
    }

    public void setSelectedFlight(FlightDto selectedFlight) {
        this.selectedFlight = selectedFlight;
    }

    public AircraftDto getSelectedPlane() {
        return selectedPlane;
    }

    public void setSelectedPlane(AircraftDto selectedPlane) {
        this.selectedPlane = selectedPlane;
    }

    public DateDto getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(DateDto departureDate) {
        this.departureDate = departureDate;
    }

    public TimeDto getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(TimeDto departureTime) {
        this.departureTime = departureTime;
    }

    public DateDto getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(DateDto arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public TimeDto getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(TimeDto arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public double getFirstClass() {
        return firstClass;
    }

    public void setFirstClass(double firstClass) {
        this.firstClass = firstClass;
    }

    public double getBusiness() {
        return business;
    }

    public void setBusiness(double business) {
        this.business = business;
    }

    public double getEconomy() {
        return economy;
    }

    public void setEconomy(double economy) {
        this.economy = economy;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
