package com.example.demo.dto;

public class SearchDataDto {
    private String from;
    private String to;
    private int numberOfPassengers;
    private DateDto departureDate;
    private DateDto arrivalDate;
    private boolean checked;
    private String typeT;

    public SearchDataDto() {
    }

    public SearchDataDto(String from, String to, int numberOfPassengers, DateDto departureDate,
                         DateDto arrivalDate, boolean checked, String typeT) {
        this.from = from;
        this.to = to;
        this.numberOfPassengers = numberOfPassengers;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.checked = checked;
        this.typeT = typeT;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public DateDto getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(DateDto departureDate) {
        this.departureDate = departureDate;
    }

    public DateDto getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(DateDto arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getTypeT() {
        return typeT;
    }

    public void setTypeT(String typeT) {
        this.typeT = typeT;
    }
}
