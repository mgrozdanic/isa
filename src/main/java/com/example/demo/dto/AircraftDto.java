package com.example.demo.dto;

import java.util.List;

public class AircraftDto {
    private String identifier;
    private String description;
    private String company;
    private int rows;
    private int columns;
    private List<Integer> removed;
    private List<Integer> firstClass;
    private List<Integer> business;

    public AircraftDto() {
    }

    public AircraftDto(String identifier, String description, String company, int rows, int columns,
                       List<Integer> removed, List<Integer> firstClass, List<Integer> business) {
        this.identifier = identifier;
        this.description = description;
        this.company = company;
        this.rows = rows;
        this.columns = columns;
        this.removed = removed;
        this.firstClass = firstClass;
        this.business = business;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public List<Integer> getRemoved() {
        return removed;
    }

    public void setRemoved(List<Integer> removed) {
        this.removed = removed;
    }

    public List<Integer> getFirstClass() {
        return firstClass;
    }

    public void setFirstClass(List<Integer> firstClass) {
        this.firstClass = firstClass;
    }

    public List<Integer> getBusiness() {
        return business;
    }

    public void setBusiness(List<Integer> business) {
        this.business = business;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
