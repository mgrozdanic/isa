package com.example.demo.dto;

import java.util.List;

public class LayoutItemDto {
    private String user;
    private FlightRealisationResultDto flight;
    private List<Integer> booked;

    public LayoutItemDto() {
    }

    public LayoutItemDto(String user, FlightRealisationResultDto flightRealisationResultDto, List<Integer> booked) {
        this.user = user;
        this.flight = flightRealisationResultDto;
        this.booked = booked;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public FlightRealisationResultDto getFlight() {
        return flight;
    }

    public void setFlight(FlightRealisationResultDto flightRealisationResultDto) {
        this.flight = flightRealisationResultDto;
    }

    public List<Integer> getBooked() {
        return booked;
    }

    public void setBooked(List<Integer> booked) {
        this.booked = booked;
    }
}
