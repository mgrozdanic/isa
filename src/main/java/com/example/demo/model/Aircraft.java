package com.example.demo.model;

import com.example.demo.dto.AircraftDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "aircraft")
public class Aircraft  implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "aircraftid", unique = true, nullable = false)
    private Long aircraftId;
    @Column(name = "description", nullable = false)
    private String description;
    @Column(name = "identifier", nullable = false)
    private String identifier;
    @ManyToOne
    private AvioCompany aircraftOwner;
    @Column(name = "rowsno", nullable = false)
    private int rowsNo;
    @Column(name = "columnsno", nullable = false)
    private int columnsNo;
    @ElementCollection
    @CollectionTable(name="firstclass", joinColumns=@JoinColumn(name="aircraftid"))
    @Column(name="fclass")
    private List<Integer> firstClass;
    @ElementCollection
    @CollectionTable(name="bussiness", joinColumns=@JoinColumn(name="aircraftid"))
    @Column(name="bness")
    private List<Integer> bussiness;
    @ElementCollection
    @CollectionTable(name="removedseats", joinColumns=@JoinColumn(name="aircraftid"))
    @Column(name="rseats")
    private List<Integer> removedSeats;

    public Aircraft() {
    }

    public Aircraft(String description, String identifier, AvioCompany aircraftOwner,
                    int rowsNo, int columnsNo, List<Integer> firstClass, List<Integer> bussiness,
                    List<Integer> removedSeats) {
        //this.aircraftId = aircraftId;
        this.description = description;
        this.identifier = identifier;
        this.aircraftOwner = aircraftOwner;
        this.rowsNo = rowsNo;
        this.columnsNo = columnsNo;
        //this.gaps = gaps;
        this.firstClass = firstClass;
        this.bussiness = bussiness;
        this.removedSeats = removedSeats;
    }

    public Aircraft(AircraftDto selectedPlane, AvioCompany company) {
        this.description = selectedPlane.getDescription();
        this.identifier = selectedPlane.getIdentifier();
        this.aircraftOwner = company;
        this.rowsNo = selectedPlane.getRows();
        this.columnsNo = selectedPlane.getColumns();
        this.firstClass = selectedPlane.getFirstClass();
        this.bussiness = selectedPlane.getBusiness();
        this.removedSeats = selectedPlane.getRemoved();
    }

    public AvioCompany getAircraftOwner() {
        return aircraftOwner;
    }

    public void setAircraftOwner(AvioCompany aircraftOwner) {
        this.aircraftOwner = aircraftOwner;
    }

    public int getRowsNo() {
        return rowsNo;
    }

    public void setRowsNo(int rowsNo) {
        this.rowsNo = rowsNo;
    }

    public int getColumnsNo() {
        return columnsNo;
    }

    public void setColumnsNo(int columnsNo) {
        this.columnsNo = columnsNo;
    }

    public List<Integer> getFirstClass() {
        return firstClass;
    }

    public void setFirstClass(List<Integer> firstClass) {
        this.firstClass = firstClass;
    }

    public List<Integer> getBussiness() {
        return bussiness;
    }

    public void setBussiness(List<Integer> bussiness) {
        this.bussiness = bussiness;
    }

    public List<Integer> getRemovedSeats() {
        return removedSeats;
    }

    public void setRemovedSeats(List<Integer> removedSeats) {
        this.removedSeats = removedSeats;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getAircraftId() {
        return aircraftId;
    }

    public void setAircraftId(Long aircraftId) {
        this.aircraftId = aircraftId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
