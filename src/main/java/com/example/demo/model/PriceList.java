package com.example.demo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "pricelist")
public class PriceList  implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pricelistid", unique = true, nullable = false)
    private Long priceListId;
    @ManyToOne
    private AvioCompany avioCompany;
    @Column(name = "bussiness", nullable = false)
    private double bussiness;
    @Column(name = "economy", nullable = false)
    private double economy;
    @Column(name = "firstClass", nullable = false)
    private double firstClass;

    public PriceList() {
    }

    public PriceList(AvioCompany avioCompany, double bussiness, double economy, double firstClass) {
        this.avioCompany = avioCompany;
        this.bussiness = bussiness;
        this.economy = economy;
        this.firstClass = firstClass;
    }

    public double getBussiness() {
        return bussiness;
    }

    public void setBussiness(double bussiness) {
        this.bussiness = bussiness;
    }

    public double getEconomy() {
        return economy;
    }

    public void setEconomy(double economy) {
        this.economy = economy;
    }

    public double getFirstClass() {
        return firstClass;
    }

    public void setFirstClass(double firstClass) {
        this.firstClass = firstClass;
    }

    public Long getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(Long priceListId) {
        this.priceListId = priceListId;
    }

    public AvioCompany getAvioCompany() {
        return avioCompany;
    }

    public void setAvioCompany(AvioCompany avioCompany) {
        this.avioCompany = avioCompany;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
