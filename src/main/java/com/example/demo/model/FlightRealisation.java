package com.example.demo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "flightrealisation")
public class FlightRealisation  implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "flightrealisationid", unique = true, nullable = false)
    private Long flightRealisationId;
    @ManyToOne
    private Aircraft aircraft;
    @ManyToOne
    private Flight flight;
    //private LayoutItem layout;          // <-- current aircraft state here
    @Temporal(TemporalType.TIMESTAMP)
    private Date departureDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date arrivalDate;
    @Column(name = "bussiness", nullable = false)
    private double bussiness;
    @Column(name = "economy", nullable = false)
    private double economy;
    @Column(name = "firstClass", nullable = false)
    private double firstClass;
    @Column(name = "active", nullable = false)
    private boolean active;

    public FlightRealisation() {
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getFlightRealisationId() {
        return flightRealisationId;
    }

    public void setFlightRealisationId(Long flightRealisationId) {
        this.flightRealisationId = flightRealisationId;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getBussiness() {
        return bussiness;
    }

    public void setBussiness(double bussiness) {
        this.bussiness = bussiness;
    }

    public double getEconomy() {
        return economy;
    }

    public void setEconomy(double economy) {
        this.economy = economy;
    }

    public double getFirstClass() {
        return firstClass;
    }

    public void setFirstClass(double firstClass) {
        this.firstClass = firstClass;
    }
}
