package com.example.demo.service.interfaces;

import com.example.demo.model.Flight;
import com.example.demo.model.FlightRealisation;

import java.util.Date;
import java.util.List;

public interface FlightRealisationInterface {
    FlightRealisation save(FlightRealisation flightRealisation);
    FlightRealisation getMatching1(Flight flight, Date departureDate, Date arrivalDate);
    List<FlightRealisation> getMatching2(Flight flight, Date departureDate);
    List<FlightRealisation> findByFlight(Flight flight);
}
