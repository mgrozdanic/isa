package com.example.demo.service.interfaces;

import com.example.demo.model.FlightRealisation;
import com.example.demo.model.LayoutItem;

import java.util.List;

public interface LayoutServiceInterface {
    LayoutItem findByFlightRealisationAndSeat(FlightRealisation flightRealisation, int seat);
    List<LayoutItem> findByFlightRealisation(FlightRealisation flightRealisation);
    LayoutItem save(LayoutItem layoutItem);
}
