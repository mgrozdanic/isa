package com.example.demo.service.interfaces;

import com.example.demo.model.Aircraft;
import com.example.demo.model.AvioCompany;

import java.util.List;

public interface AircraftServiceInterface {
    Aircraft findByIdentifier(String identifier);
    Aircraft save(Aircraft aircraft);
    List<Aircraft> findAll(AvioCompany avioCompany);
}
