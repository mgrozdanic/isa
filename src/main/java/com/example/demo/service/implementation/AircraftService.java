package com.example.demo.service.implementation;

import com.example.demo.model.Aircraft;
import com.example.demo.model.AvioCompany;
import com.example.demo.repository.AircraftRepository;
import com.example.demo.service.interfaces.AircraftServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AircraftService implements AircraftServiceInterface {
    @Autowired
    public AircraftRepository aircraftRepository;

    @Override
    public Aircraft findByIdentifier(String identifier) {
        return aircraftRepository.findByIdentifier(identifier);
    }

    @Override
    public Aircraft save(Aircraft aircraft) {
        return aircraftRepository.save(aircraft);
    }

    @Override
    public List<Aircraft> findAll(AvioCompany avioCompany) {
        return aircraftRepository.findAllByAircraftOwner(avioCompany);
    }
}
