package com.example.demo.service.implementation;

import com.example.demo.model.Flight;
import com.example.demo.model.FlightRealisation;
import com.example.demo.repository.FlightRealisationRepository;
import com.example.demo.service.interfaces.FlightRealisationInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class FlightRealisationService implements FlightRealisationInterface {

    @Autowired
    private FlightRealisationRepository flightRealisationRepository;

    @Override
    public FlightRealisation save(FlightRealisation flightRealisation) {
        return flightRealisationRepository.save(flightRealisation);
    }

    @Override
    public FlightRealisation getMatching1(Flight flight, Date departureDate, Date arrivalDate) {
        return flightRealisationRepository.getMatching1(flight, departureDate, arrivalDate);
    }

    @Override
    public List<FlightRealisation> getMatching2(Flight flight, Date departureDate) {
        return flightRealisationRepository.getMatching2(flight, departureDate);
    }

    @Override
    public List<FlightRealisation> findByFlight(Flight flight) {
        return flightRealisationRepository.findByFlight(flight);
    }
}
