package com.example.demo.service.implementation;

import com.example.demo.model.FlightRealisation;
import com.example.demo.model.LayoutItem;
import com.example.demo.repository.LayoutRepository;
import com.example.demo.service.interfaces.LayoutServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LayoutItemService implements LayoutServiceInterface {

    @Autowired
    private LayoutRepository layoutRepository;

    @Override
    public LayoutItem findByFlightRealisationAndSeat(FlightRealisation flightRealisation, int seat) {
        return layoutRepository.findByFlightRealisationAndSeat(flightRealisation, seat);
    }

    @Override
    public List<LayoutItem> findByFlightRealisation(FlightRealisation flightRealisation) {
        return layoutRepository.findByFlightRealisation(flightRealisation);
    }

    @Override
    public LayoutItem save(LayoutItem layoutItem) {
        return layoutRepository.save(layoutItem);
    }
}
