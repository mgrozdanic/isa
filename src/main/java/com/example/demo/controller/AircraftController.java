package com.example.demo.controller;

import com.example.demo.dto.AircraftDto;
import com.example.demo.dto.FlightDto;
import com.example.demo.model.Aircraft;
import com.example.demo.model.AvioCompany;
import com.example.demo.repository.AircraftRepository;
import com.example.demo.service.implementation.AircraftService;
import com.example.demo.service.interfaces.AircraftServiceInterface;
import com.example.demo.service.interfaces.AvioCompanyServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RequestMapping
@RestController
public class AircraftController {
    @Autowired
    public AircraftServiceInterface aircraftServiceInterfacer;

    @Autowired
    public AvioCompanyServiceInterface avioCompanyServiceInterface;

    @RequestMapping(value = "/aircraft/create", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AircraftDto> create(@RequestBody AircraftDto aircraftDto) {
        Aircraft conflict = aircraftServiceInterfacer.findByIdentifier(aircraftDto.getIdentifier());
        if (conflict != null){
            return new ResponseEntity<>(new AircraftDto("conflict", "", "", 0, 0, new ArrayList<Integer>()
                    , new ArrayList<Integer>(), new ArrayList<Integer>()), HttpStatus.OK);
        }
        AvioCompany avioCompany = avioCompanyServiceInterface.findByName(aircraftDto.getCompany());
        Aircraft aircraft = new Aircraft(aircraftDto.getDescription(), aircraftDto.getIdentifier(), avioCompany,
                aircraftDto.getRows(), aircraftDto.getColumns(), aircraftDto.getFirstClass(), aircraftDto.getBusiness(),
                aircraftDto.getRemoved());
        Aircraft a = aircraftServiceInterfacer.save(aircraft);
        return new ResponseEntity<>(aircraftDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/aircraft/getAircraft/{company}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AircraftDto>> getAircraft(@PathVariable("company") String company) {
        List<Aircraft> allAircrafts = aircraftServiceInterfacer.findAll(avioCompanyServiceInterface.findByName(company));
        List<AircraftDto> aircraftDtos = new ArrayList<AircraftDto>();
        if (allAircrafts == null){
            return new ResponseEntity<>(aircraftDtos, HttpStatus.OK);
        }
        for (Aircraft a : allAircrafts){
            aircraftDtos.add(new AircraftDto(a.getIdentifier(), a.getDescription(), a.getAircraftOwner().getName(),
                    a.getRowsNo(), a.getColumnsNo(), a.getRemovedSeats(), a.getFirstClass(), a.getBussiness()));
        }
        return new ResponseEntity<>(aircraftDtos, HttpStatus.OK);
    }
}
