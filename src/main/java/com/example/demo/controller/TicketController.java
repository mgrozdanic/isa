package com.example.demo.controller;

import com.example.demo.dto.LayoutItemDto;
import com.example.demo.model.AvioCompany;
import com.example.demo.model.Flight;
import com.example.demo.model.FlightRealisation;
import com.example.demo.model.LayoutItem;
import com.example.demo.service.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin
@RequestMapping
@RestController
public class TicketController {
    @Autowired
    private FlightRealisationInterface flightRealisationInterface;

    @Autowired
    private AvioCompanyServiceInterface avioCompanyServiceInterface;

    @Autowired
    private AircraftServiceInterface aircraftServiceInterface;

    @Autowired
    private RegisteredUserServiceInterface registeredUserServiceInterface;

    @Autowired
    private FlightServiceInterface flightServiceInterface;

    @Autowired
    private LayoutServiceInterface layoutServiceInterface;

    @RequestMapping(value = "/ticket/buy", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LayoutItemDto> buy(@RequestBody LayoutItemDto layoutItemDto) {
        System.out.println("\n\n"+layoutItemDto.getFlight()+"\n\n");
        if (layoutItemDto.getFlight().getType().equals("One way")){
            Date d = new Date(Integer.parseInt(layoutItemDto.getFlight().getDepartureDate().getYear()),
                    Integer.parseInt(layoutItemDto.getFlight().getDepartureDate().getMonth()),
                    Integer.parseInt(layoutItemDto.getFlight().getDepartureDate().getDay()),
                    Integer.parseInt(layoutItemDto.getFlight().getDepartureTime().getHour()),
                    Integer.parseInt(layoutItemDto.getFlight().getDepartureTime().getMinute()));
            AvioCompany ac = avioCompanyServiceInterface.findByName(layoutItemDto.getFlight().getCompany());
            Flight f = flightServiceInterface.findFlight(layoutItemDto.getFlight().getFrom(), layoutItemDto.getFlight().getTo(), layoutItemDto.getFlight().getFromCode(), layoutItemDto.getFlight().getToCode(), ac);
            System.out.println("Flight:"+f);
            System.out.println("AvioCompany:"+ac);
            //return new ResponseEntity<>(null, HttpStatus.OK);
            FlightRealisation fr = findRealization(f, ac, d);

            for (int i : layoutItemDto.getBooked()){
                if (layoutServiceInterface.findByFlightRealisationAndSeat(fr, i) != null){
                    return new ResponseEntity<>(new LayoutItemDto("conflict", null, new ArrayList<Integer>()), HttpStatus.OK);
                }
            }
            for (int i : layoutItemDto.getBooked()){
                LayoutItem layoutItem = new LayoutItem();
                layoutItem.setAircraft(fr.getAircraft());
                layoutItem.setFlightRealisation(fr);
                layoutItem.setSeat(i);
                layoutItem.setUser(registeredUserServiceInterface.findRegByUsername(layoutItemDto.getUser()));
                LayoutItem li = layoutServiceInterface.save(layoutItem);
            }
        } else{

        }

        return new ResponseEntity<>(layoutItemDto, HttpStatus.OK);
    }

    private FlightRealisation findRealization(Flight f, AvioCompany company, Date dateToCompare){
        List<FlightRealisation> realisations = flightRealisationInterface.findByFlight(f);
        for (FlightRealisation fr: realisations) {
            Date date = new Date(fr.getDepartureDate().getYear(), fr.getDepartureDate().getMonth(),
                    fr.getDepartureDate().getDay(), fr.getDepartureDate().getHours(), fr.getDepartureDate().getMinutes());
            System.out.println("\n\n\n"+date);
            System.out.println(dateToCompare+"\n\n\n");
            if (date.compareTo(dateToCompare) == 0){
                return fr;
            }
        }
        return null;
    }
}
