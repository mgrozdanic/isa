package com.example.demo.controller;

import com.example.demo.dto.*;
import com.example.demo.model.*;
import com.example.demo.service.interfaces.AircraftServiceInterface;
import com.example.demo.service.interfaces.AvioCompanyServiceInterface;
import com.example.demo.service.interfaces.FlightRealisationInterface;
import com.example.demo.service.interfaces.FlightServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin
@RequestMapping
@RestController
public class FlightRealisationController {

    @Autowired
    private FlightRealisationInterface flightRealisationInterface;

    @Autowired
    private AvioCompanyServiceInterface avioCompanyServiceInterface;

    @Autowired
    private AircraftServiceInterface aircraftServiceInterface;

    @Autowired
    private FlightServiceInterface flightServiceInterface;

    @RequestMapping(value = "/flightRealisation/create", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FlightRealisationDto> create(@RequestBody FlightRealisationDto flightRealisationDto) {
        FlightRealisation flightRealisation = new FlightRealisation();
        AvioCompany company = avioCompanyServiceInterface.findByName(flightRealisationDto.getCompany());
        Aircraft aircraft = aircraftServiceInterface.findByIdentifier(flightRealisationDto.getSelectedPlane().getIdentifier());
        flightRealisation.setAircraft(aircraft);
        Flight flight = flightServiceInterface.findFlight(flightRealisationDto.getSelectedFlight().getFrom(),
                flightRealisationDto.getSelectedFlight().getTo(), flightRealisationDto.getSelectedFlight().getFromCode(),
                flightRealisationDto.getSelectedFlight().getToCode(), company);
        flightRealisation.setFlight(flight);
        flightRealisation.setBussiness(flightRealisationDto.getBusiness());
        flightRealisation.setFirstClass(flightRealisationDto.getFirstClass());
        flightRealisation.setEconomy(flightRealisationDto.getEconomy());
        int yearD = Integer.parseInt(flightRealisationDto.getDepartureDate().getYear());
        int monthD = Integer.parseInt(flightRealisationDto.getDepartureDate().getMonth());
        int dayD = Integer.parseInt(flightRealisationDto.getDepartureDate().getDay());
        int hourD = Integer.parseInt(flightRealisationDto.getDepartureTime().getHour());
        int minuteD = Integer.parseInt(flightRealisationDto.getDepartureTime().getMinute());

        int yearA = Integer.parseInt(flightRealisationDto.getArrivalDate().getYear());
        int monthA = Integer.parseInt(flightRealisationDto.getArrivalDate().getMonth());
        int dayA = Integer.parseInt(flightRealisationDto.getArrivalDate().getDay());
        int hourA = Integer.parseInt(flightRealisationDto.getArrivalTime().getHour());
        int minuteA = Integer.parseInt(flightRealisationDto.getArrivalTime().getMinute());

        flightRealisation.setDepartureDate(new Date(yearD, monthD, dayD, hourD, minuteD));
        flightRealisation.setArrivalDate(new Date(yearA, monthA, dayA, hourA, minuteA));
        flightRealisation.setActive(true);
        flightRealisationInterface.save(flightRealisation);
        return new ResponseEntity<>(flightRealisationDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/flightRealisation/getMatching", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FlightRealisationResultDto>> getMatching(@RequestBody SearchDataDto searchDataDto) {
        List<Flight> flights = flightServiceInterface.findFlightFromTo(searchDataDto.getFrom(), searchDataDto.getTo());
        ArrayList<FlightRealisationResultDto> flightRealisationsFiltered = new ArrayList<>();

        int yearD = Integer.parseInt(searchDataDto.getDepartureDate().getYear());
        int monthD = Integer.parseInt(searchDataDto.getDepartureDate().getMonth());
        int dayD = Integer.parseInt(searchDataDto.getDepartureDate().getDay());
        Date departure = new Date(yearD, monthD, dayD);

        System.out.println("\n--\n"+searchDataDto.getDepartureDate().getYear()+searchDataDto.getDepartureDate().getMonth()+
                searchDataDto.getDepartureDate().getDay()+"\n--\n");

        if (searchDataDto.isChecked()){
            int yearA = Integer.parseInt(searchDataDto.getArrivalDate().getYear());
            int monthA = Integer.parseInt(searchDataDto.getArrivalDate().getMonth());
            int dayA = Integer.parseInt(searchDataDto.getArrivalDate().getDay());
            Date arrival = new Date(yearA, monthA, dayA);// ovo nije gotovo, trazi let iz destinacija za nazad
            /*for (Flight f : flights){
                List<FlightRealisation> flightRealisationList = flightRealisationInterface.findByFlight(f);
                for(FlightRealisation fr : flightRealisationList){
                    if (departure.compareTo(new Date(fr.getDepartureDate().getYear(), fr.getDepartureDate().getMonth(),
                            fr.getDepartureDate().getDay())) == 0){
                        flightRealisationsFiltered.add(fr);
                    }
                }
            }*/
        } else {
            for (Flight f : flights){
                List<FlightRealisation> flightRealisationList = flightRealisationInterface.findByFlight(f);
                for(FlightRealisation fr : flightRealisationList){

                    Date frDate = new Date(fr.getDepartureDate().getYear(), fr.getDepartureDate().getMonth(),
                            fr.getDepartureDate().getDay());

                    if (departure.compareTo(frDate) == 0){
                        System.out.println("\n\n----" + "-----\n\n");
                        double price = fr.getFirstClass();
                        List<Integer> unavailable = new ArrayList<>();
                        if (searchDataDto.getTypeT().equals("ECONOMY")){
                            price = fr.getEconomy();
                            unavailable.addAll(fr.getAircraft().getRemovedSeats());
                            unavailable.addAll(fr.getAircraft().getBussiness());
                            unavailable.addAll(fr.getAircraft().getFirstClass());
                        } else if (searchDataDto.getTypeT().equals("BUSINESS")){
                            price = fr.getBussiness();
                            unavailable.addAll(fr.getAircraft().getRemovedSeats());
                            //unavailable.addAll(fr.getAircraft().()); metod za economy
                            unavailable.addAll(fr.getAircraft().getFirstClass());
                        } else{
                            unavailable.addAll(fr.getAircraft().getRemovedSeats());
                            unavailable.addAll(fr.getAircraft().getBussiness());
                            //isto za economy, a kod svih jos zauzeta sedista
                        }
                        FlightRealisationResultDto resultDto = new FlightRealisationResultDto(f.getFrom(), f.getTo(),
                                new TimeDto(String.valueOf(fr.getDepartureDate().getHours()),
                                        String.valueOf(fr.getDepartureDate().getMinutes()), "0"),
                                new DateDto(String.valueOf(fr.getDepartureDate().getYear()),
                                        String.valueOf(fr.getDepartureDate().getMonth()),
                                        String.valueOf(fr.getDepartureDate().getDay())),
                                new TimeDto(String.valueOf(fr.getArrivalDate().getHours()),
                                        String.valueOf(fr.getArrivalDate().getMinutes()), "0"),
                                new DateDto(String.valueOf(fr.getArrivalDate().getYear()),
                                        String.valueOf(fr.getArrivalDate().getMonth()),
                                        String.valueOf(fr.getArrivalDate().getDay())),
                                "One way", searchDataDto.getTypeT(), fr.getAircraft().getAircraftOwner().getName(),
                                price, new FlightDto(f.getFrom(), f.getFromCode(), f.getTo(), f.getToCode(),
                        f.getDistance(), f.getAvioCompany().getName()), unavailable, fr.getAircraft().getRowsNo(),
                                fr.getAircraft().getColumnsNo(), fr.getFlight().getFromCode(), fr.getFlight().getToCode());
                        flightRealisationsFiltered.add(resultDto);
                    }
                }
            }

        }
        return new ResponseEntity<>(flightRealisationsFiltered, HttpStatus.OK);
    }
}
