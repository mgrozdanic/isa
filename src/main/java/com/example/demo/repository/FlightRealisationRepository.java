package com.example.demo.repository;

import com.example.demo.model.Flight;
import com.example.demo.model.FlightRealisation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface FlightRealisationRepository extends JpaRepository<FlightRealisation, Long> {

    @Query("SELECT f FROM FlightRealisation f WHERE f.flight = :flight AND f.departureDate = :departureDate" +
            " AND f.arrivalDate = :arrivalDate")
    FlightRealisation getMatching1(@Param("flight") Flight flight, @Param("departureDate") Date departureDate
            , @Param("arrivalDate") Date arrivalDate);

    @Query("SELECT f FROM FlightRealisation f WHERE f.flight = :flight AND f.departureDate = :departureDate")
    List<FlightRealisation> getMatching2(@Param("flight") Flight flight, @Param("departureDate") Date departureDate);

    List<FlightRealisation> findByFlight(Flight flight);
}
