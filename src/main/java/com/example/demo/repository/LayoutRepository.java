package com.example.demo.repository;

import com.example.demo.model.FlightRealisation;
import com.example.demo.model.LayoutItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LayoutRepository extends JpaRepository<LayoutItem, Long> {
    List<LayoutItem> findByFlightRealisation(FlightRealisation flightRealisation);

    @Query("SELECT l FROM LayoutItem l WHERE l.flightRealisation = ?1 AND l.seat = ?2")
    LayoutItem findByFlightRealisationAndSeat(FlightRealisation flightRealisation, int seat);
}
