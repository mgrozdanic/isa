package com.example.demo.repository;

import com.example.demo.model.Aircraft;
import com.example.demo.model.AvioCompany;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AircraftRepository extends JpaRepository<Aircraft, Long>{
    Aircraft findByIdentifier(String aircraft);
    List<Aircraft> findAllByAircraftOwner(AvioCompany avioCompany);
}
